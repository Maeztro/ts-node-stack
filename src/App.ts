import {itemModel, accountModel , userProfileModel} from './config/config';
import {AccountsRoutes} from './modules/Users/AccountsRoutes';
import {UserController} from './modules/Users/UserController';
import {ItemController} from './modules/Items/ItemController';
import {ItemIoQuery, ItemRoutes} from './modules/Items/ItemRoutes';
import {Server} from './modules/Server/Server';
import {MongoConnect} from './modules/DatabaseConnect/MongoConnect'

const dbUrl = process.env.MONGO_CONNECT_URL;
const mongoDatabase = new MongoConnect();

mongoDatabase.connect( dbUrl )
    .then( () => {

        const itemList: ItemController = new ItemController(mongoDatabase.getDb());
        const userList: UserController = new UserController(mongoDatabase.getDb());

        const accountRoutes = new AccountsRoutes( {
            itemList:userList,
            route:"Accounts" ,
            model: accountModel
        });

        const itemRoutes = new ItemRoutes({
            itemList:itemList,
            route:"Items" ,
            model: itemModel
        });

        const itemIoQuery = new ItemIoQuery({itemList:itemList, route:"Items" , model: itemModel});

        const routes = [
            {name:"itemApi", route: itemRoutes , ioRoute:itemIoQuery},
            {name:"accountsApi", route: accountRoutes }
        ];

        const server = new Server({
            port: process.env.PORT || 3000,
            cors: true,
            route: routes
        });

        server.startServer();
        server.runTests(false);

        // todo: create a config profile that regroup the front end app name and its models and lists for routes

});
