
export const newAccountEmailTemplate = `

    <html>
    
        <style>
        .title {
            font-family: Arial , sans-serif;
            font-size: 2em;
            color: #1766a9;
        }
        .paragraph {
            font-family: Verdana , serif;
            font-size: 0.9em;
            color: #061f35;
        }
        
    </style>
    
    <body>
        <h1 class="title">Welcome to portfolio demo</h1>
        <p class="paragraph">your account has been successfully created</p>
    </body>
      
    </html>
`;


export const userProfileModel = [
    { name: "string", editable: "boolean", visible:"boolean" },
    { userProfileImage: "string", editable: "boolean", visible:"boolean" },
    { description: "string", editable: "boolean", visible:"boolean" },
    { active: "boolean", editable: "boolean", visible:"boolean" },
];

export const accountModel = {
    _id: "string",
    username: "string",
    email: "string",
    password: "string",
    profile: userProfileModel
}

export const itemModel = {
    _id: "string",
    username: "string",
    userId: "string",
    userProfileImage: "string",
    upVotes: "number",
    downVotes: "number",
    editMode: "boolean",
    content: {
        title: "string",
        subtitle: "string",
        url: "string",
        link: "string",
        description: "string",
        category: "string",
    }
};

/**
 *  for test purpose only :D
 * */

export const stressModel = {
    _id: "string",
    username: "string",
    email: "string",
    password: "string",
    content : itemModel.content,
    insanity: [
        itemModel.content
    ]
}

export const nestedModel = {
    singleField: "string",
    arrayField:[
        {singleField: "string"}
    ],
    objectField:{
        singleField:"string"
    },
    arrayFieldWithNestedArray:[
        [
            {singleField: "string"}
        ]
    ],
    objectFieldWithNestedArray:{
        singleField:[
            {singleField: "string"}
        ]
    },
    objectFieldWithNestedObjectField:{
        singleField: {singleField: "string"}
    },
}
