import {Request, Response} from 'express';
import {DiceTool} from './DiceTool';

const express = require('express');

const mockDiceTool: DiceTool = new DiceTool({
    success: 7,
    critical: 10,
    botch: 1,
});

export class DiceRoutes extends express.Router{

    protected diceTool: DiceTool = mockDiceTool;

    constructor (){
        super();

        this.get(`/roll:amount/d:size`, (req: Request, res: Response) => {
            // res.send(`${JSON.stringify( this.diceTool.showActionReport(req.params.amount,req.params.size))}`)
        });

    }

}

export class DiceIoQuery {

    protected diceTool: DiceTool = mockDiceTool;

    constructor (){}

    public ioQuery(data: any) {
        return this.diceTool.showActionReport(data.roll, data.size);
    }
}
