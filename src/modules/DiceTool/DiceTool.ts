export enum DiceResult{
    Botch = "botch",
    Miss = "miss",
    Success = "success",
    Critical = "critical",
}

export class DiceTool {

    protected success: number;
    protected critical: number;
    protected botch: number;
    protected miss: number;

    constructor(config: any){
        this.success = config.success;
        this.critical = config.critical;
        this.botch = config.botch;
    }

    public showActionReport(rolls: number , dice: number){
        let result = this.rollAction(rolls , dice);
        let report = `Action result: success: ${this.countDiceResult(result,'success')} critical: ${this.countDiceResult(result,'critical')} miss: ${this.countDiceResult(result,'miss')} botch: ${this.countDiceResult(result,'botch')} [ ${this.showDiceResultList(result)} ]`;
        console.log(report);
        return report;
    }

    protected showDiceResultList(results: any,){
        let resultList: any[] = [];
        results.forEach( (result: any) => {
            resultList.push(result.diceRolled);
        });
        return resultList;
    }
    protected countDiceResult(results: any, resultName: string) {
        let count = 0;
        results.forEach( (result: any) => {
            if (result.resultEffect === resultName) {
                count++;
            }
        });
        return count;
    }

    protected rollAction(rolls: number , dice: number) {
        let results = [];
        for (let i = 0 ; i < rolls; i++) {
            const diceRolled = this.rollDice(dice);
            const result = {diceRolled: diceRolled  , resultEffect:this.isSuccess(diceRolled)};
            results.push(result)
        }
        return results;
    }

    protected rollDice(dice: number) {
        return Math.floor(Math.random() * dice ) + 1;
    }

    protected isSuccess(amount: number) {
        let result: DiceResult = DiceResult.Miss;
        if(amount >= this.success && amount < this.critical) {
            result = DiceResult.Success;
        } else if (amount >= this.critical) {
            result = DiceResult.Critical;
        } else if (amount <= this.botch) {
            result = DiceResult.Botch;
        }
        return result;
    }

}
