import {Request, Response} from 'express';
import {DTOValidation} from '../DtoValidation/DTOValidation';
import {ItemController} from './ItemController';
import {isAuth} from '../Users/AccountsRoutes'

const express = require('express');

export class ItemRoutes extends express.Router{

    protected itemController: ItemController;
    protected itemModel: any;
    protected validation: DTOValidation = new DTOValidation();

    constructor (config?: any){
        super();

        this.itemController = config.itemList;
        this.itemModel = config.model;

        this.get(`/getModel`, (req: Request, res: Response) => {
            res.send(`${JSON.stringify(this.itemModel)}`);
        });

        this.get(`/getAll/${config.route}`, (req: Request, res: Response) => {
            console.log("GET ALL");
            this.itemController.getAll({collection: config.route })
                .then( (item: any[]) => {
                    // console.log("RESPONSE?" , item);
                    res.send(`${JSON.stringify(item)}`)
                });
        });

        this.get(`/getByAuthor/${config.route}/:author`, (req: Request, res: Response) => {
            console.log("GET BY AUTHOR");
            this.itemController.getByAuthor({collection: config.route , field: req.params.author })
                .then( (item: any[]) => {
                    console.log("RESPONSE?" , req.body , req.params.query);
                    res.send(`${JSON.stringify(item)}`)
                });
        });

        this.get(`/getOne/${config.route}/:id`, (req: Request, res: Response) => {
            this.itemController.getById({collection: config.route , id: req.params.id })
                .then( (item: any) => {
                    console.log("RESPONSE?" , req.body , req.params.query);
                    res.send(`${JSON.stringify(item)}`)
                });

        });

        this.post(`/postOne/${config.route}`, isAuth , (req: Request,res: Response) => {

            console.log("NEW ITEM" , req.body)

            if(
                this.validation.validateDto( this.itemModel,  req.body , true)
                // this.validation.validateDtoArray( this.itemModel,  req.body )
            ) {
                // copy the new item into an empty object to allow mongoDB to assign an id instead of a blank string

                const newItem = {};

                //todo this wont be necessary since _id is now separated from the data, {_id="",data:req.body}
                for(let [key] of Object.entries(req.body)) {
                    if(key === "_id") {
                        //let Mongo deal with the id
                    } else {
                        newItem[key] = req.body[key];
                    }
                }

                this.itemController.create({collection: config.route, value: newItem})
                    .then(result => {
                        // update the object with its new id?
                        res.send({status: 200});
                    })
                    .catch(err => console.log(err));
            } else {
                console.log("invalid request")
                res.send({status:500 , message: "Invalid Request"});
            }
        });

        this.put(`/editOne/${config.route}`, isAuth , (req: Request,res: Response) => {

            //todo : eventually validate if the user id match the item id here as well for extra safety

            if(
                this.validation.validateDto( this.itemModel, req.body.oldItem ) &&
                this.validation.validateDto( this.itemModel, req.body.newItem )
            ) {

                const editedItem = {};

                for(let [key] of Object.entries(req.body.newItem)) {
                    if(key === "_id") {
                        // let Mongo deal with the id
                    } else {
                        editedItem[key] = req.body.newItem[key];
                    }
                }

                this.itemController.edit({collection: config.route  , oldItem: req.body.oldItem , newItem: editedItem})
                    .then( result => {
                        res.send({status:200});
                    })
                    .catch(err => console.log(err));
            } else {
                res.send({status:500});
            }
        });

        this.delete(`/deleteOne/${config.route}/:id`, isAuth , (req: Request,res: Response) => {
            // console.log("DELETE ITEM", req.params.id, `/deleteOne/${config.route}/:id`);
            this.itemController.delete({collection: config.route , id: req.params.id})
                .then(result => {
                    console.log("QUERY RESULT:" , result.result);
                    res.send({status:200});
                })
                .catch(err => console.log(err));
        })

        this.post(`/deleteAllFromUser`, isAuth , (req: Request,res: Response) => {

            this.itemController.deleteAllFromUserId({collection: config.route , id: req.body.userId})
                .then(result => {
                    console.log("QUERY RESULT:" , result.result);
                    res.send({message: `all posts cleared from ${req.body.userId}` , status:200});
                })
                .catch(err => console.log(err));
        })

    }

}

export class ItemIoQuery {

    protected itemController;

    constructor (config?: any){
        this.itemController = config.itemList;
    }

    public ioQuery(data: any) {
        this.itemController.getById("item" + data.id).then( (item: any) => {
            console.log( JSON.stringify(item) );
        });
    }
}

