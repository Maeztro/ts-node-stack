import {ItemQuery} from './ItemQuery';
import {ObjectId} from 'mongodb';

export class ItemController {
    protected database;

    constructor(database){
        this.database = database;
    }

    public getAll(query: ItemQuery):any {
        return this.database.collection(query.collection).find().toArray();
    }

    public getById(query: ItemQuery):any {
        // console.log("GET ITEM RECIEVED",query);
        return this.database.collection(query.collection).find({_id:query.id}).toArray();
    }

    public getByAuthor(query: ItemQuery):any {
        // console.log("GET ITEM RECIEVED",query);
        return this.database.collection(query.collection).find({author:query.field}).toArray();
    }

    public create(query: ItemQuery):any {
        console.log("CREATE RECIEVED",query);
        return this.database.collection(query.collection).insertOne(query.value);
    }

    public edit(query: any):any {
        // console.log("EDIT RECIEVED",query.oldItem , query.newItem);
        console.log("OLD ITEM ID: " , query.oldItem._id);
        return this.database.collection(query.collection).updateOne({_id:new ObjectId(query.oldItem._id)}, {$set: query.newItem});
    }

    public delete(query: ItemQuery):any {
        // console.log("DELETE RECIEVED",query.id , query.collection);
        return this.database.collection(query.collection).deleteOne({_id: new ObjectId(query.id)});
    }

    public deleteAllFromUserId(query: ItemQuery):any {
        // console.log("deleteAllFromUserId RECIEVED",query.id , query.collection);
        return this.database.collection(query.collection).deleteMany({authorId:query.id});

    }

}

