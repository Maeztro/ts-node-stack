export class DTOValidation {
    constructor(){}

    /* todo: split the validations smaller functions each validating a type of dto
       todo: have the model call the proper validator function in each field that are required to validate
       todo: return true in fields that require no validations
    */

    /**
     *  DEPRECIATED
     * */
    public oldValidateDto(dtoModel: any ,dto: any) {

        // console.log("VALIDATING:" , dto);

        if(!this.arrayIsSameLength(dtoModel,dto)) {
        // console.log("not same length??");
        // console.log("model",dtoModel);
        // console.log("dto",dto);
            return false;
        }


        let checkList: boolean[] = [];

        for(let [modelKey, modelValue] of Object.entries(dtoModel)) {
            for(let [key, value] of Object.entries(dto)) {

                if(modelKey === key) {

                    if (typeof value === "object") {
                        if (Array.isArray(value)) {

                            if(value.length > 0) {
                                checkList.push(this.validateArray( modelValue , value));
                            } else {
                                checkList.push( true );
                            }
                        } else {

                            if (!this.arrayIsSameLength(modelValue,value)) {
                                return false;
                            } else {
                                checkList.push(this.validateDto( modelValue , value));
                            }
                        }

                    } else if( modelValue === typeof value ) {
                        // console.log("values type match?" , modelValue , (typeof value).toString() , value ,modelValue);
                        checkList.push(true);
                    } else {
                        // console.log("MISMATCH : key" , key , "DATA" , modelValue , (typeof value).toString() , value ,modelValue);
                        checkList.push(false);
                    }
                }
            }
        }

        // console.log("CHECKLIST: " , checkList);

        return checkList.every( result => result === true);
    }

    public validateDto (dtoModel: any , dto: any , logging?: boolean) {
        let checkList : boolean[] = [];
        let logs: string[] = [];

        //todo: this is becoming bloated, might need to split in smaller functions

        if (this.objectHaveSameKeys(dto , dtoModel)) {
            if(Array.isArray(dto)) {

                    dto.forEach( (dtoEntry , index) => {
                        for (let [modelKey, modelValue] of Object.entries(dtoModel[index])) {
                            for(let [key, value] of Object.entries(dtoEntry)) {

                                if( modelKey === key ) {
                                    //check if the value is an array and perform recursive validateDtoArray()
                                    /** in case of arrays*/
                                    if( Array.isArray(value) ) {
                                        const isValid = this.validateDto(dtoModel[index][key] , value , logging)
                                        checkList.push(isValid);
                                        logs.push(`${key} --- passed?: ${isValid} ( value: ${value})`);

                                    /** in case of objects that arent arrays*/
                                    } else if ( typeof value === "object" && !Array.isArray(value) ) {
                                        const isValid = this.validateDto(dtoModel[modelKey][index] , value , logging)
                                        checkList.push(isValid);
                                        logs.push(`${key} --- passed?: ${isValid} ( value: ${value})`);

                                    /** in case of anything else*/
                                    } else {
                                        const isValid = typeof(value) === modelValue;
                                        checkList.push(typeof(value) === modelValue);
                                        logs.push(`${key} ---  passed?: ${isValid} ( value: ${value})`);

                                    }
                                }

                            }
                        }
                    })

            } else if ( typeof dto === "object" && !Array.isArray(dto) ) {

                for ( let [modelKey, modelValue] of Object.entries(dtoModel) ) {
                    for( let [key, value] of Object.entries(dto) ) {

                        if( modelKey === key ) {
                            /** in case of arrays*/
                            if( Array.isArray(value) ) {

                                const isValid = this.validateDto(modelValue , value , logging)
                                checkList.push(isValid);
                                logs.push(`${key} --- passed?: ${isValid} ( value: ${value})`);

                            /** in case of objects that arent arrays*/
                            } else if ( typeof value === "object" && !Array.isArray(value) ) {
                                const isValid = this.validateDto(dtoModel[modelKey] , value , logging)
                                checkList.push(isValid);
                                logs.push(`${key} --- passed?: ${isValid} ( value: ${value})`);

                            /** in case of anything else*/
                            } else {
                                const isValid = typeof(value) === modelValue;
                                checkList.push(typeof(value) === modelValue);
                                logs.push(`${key} ---  passed?: ${isValid} ( value: ${value})`);

                            }
                        }

                    }
                }

            } else {

                for ( let [modelKey, modelValue] of Object.entries(dtoModel) ) {
                    for ( let [key, value] of Object.entries(dto) ) {

                        if ( modelKey === key ) {
                            /** in case of arrays*/
                            if ( Array.isArray(value) ) {
                                const isValid = this.validateDto(modelValue, value, logging)
                                checkList.push(isValid);
                                logs.push(`${key} --- passed?: ${isValid} ( value: ${value})`);

                            /** in case of objects that arent arrays*/
                            } else if ( typeof value === "object" && !Array.isArray(value) ) {
                                const isValid = this.validateDto(dtoModel[modelKey], value, logging)
                                checkList.push(isValid);
                                logs.push(`${key} --- passed?: ${isValid} ( value: ${value})`);

                            /** in case of anything else*/
                            } else {
                                const isValid = typeof (value) === modelValue;
                                checkList.push(typeof (value) === modelValue);
                                logs.push(`${key} ---  passed?: ${isValid} ( value: ${value})`);

                            }
                        }
                    }

                }
            }

        } else {
           checkList.push(false);
           logs.push(`model and items dont match!`);
        }

        if(logging){
            console.log( "VALIDATION LOGS:" , logs);
        }

        return checkList.every( (result: any) => result === true);
    }

    protected validateArray(modelValue: any , value: any): boolean {
        let checkList: any = [];

        value.forEach(  (item: any) => {
            checkList.push(this.validateDto( modelValue, item ));
        });

        return checkList.every( (result: any) => result === true);
    }

    protected arrayIsSameLength(dtoModel: any , dto: any) {

        let modelList: string[] = [];
        let dtoList: string[] = [];

        for(let [modelKey] of Object.entries(dtoModel)) {
            modelList.push(modelKey);
        }

        for(let [dtoKey] of Object.entries(dto)) {
            dtoList.push(dtoKey);
        }

        if(dtoList.length > 0) {

            if(modelList.length === dtoList.length) {
                return true;
            } else {
                return false;
            }

        } else {
            return true;
        }

    }

    public objectHaveSameKeys(...objects){
        const allKeys = objects.reduce((keys, object) => keys.concat(Object.keys(object)), []);
        const union = new Set(allKeys);
        return objects.every(object => union.size === Object.keys(object).length);
    }

}

