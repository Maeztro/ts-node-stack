const mongoDbModule = require('mongodb');
const MongoClient = mongoDbModule.MongoClient;

export class MongoConnect {

    protected database;

    constructor(config?: any){}

    public connect(url: any){
        return new Promise( (resolve, reject) => {
            MongoClient.connect(url)
                .then(client => {
                    console.log('Connected!');
                    this.database = client.db();
                    resolve(this.database);
                }).catch(err => {
                console.log(err);
                reject(err);
            });
        });
    }

    public getDb(){
        if (this.database) {
            // console.log("DB EXIST?", this.database);
            return this.database;
        } else {
            console.log("NO DATABASE");
        }
    }
}

