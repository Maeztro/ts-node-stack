import {Request, Response} from 'express';
import express from 'express';
import bodyParser from 'body-parser';
import {runValidationTests} from '../../tests/validationTests';

const helmet = require('helmet');

export class Server {

    protected app: any;
    protected server: any;
    protected io: any;
    protected port: number;

    constructor( config: any){
        this.port = config.port;

        this.createExpressApp();
        this.setPort(this.port);
        this.createServer(this.app);
        this.createSocket(this.server);

        if ( config.cors === true ){
            this.enableAppCors();
            this.enableIoCors();
        }

        this.setHttpRoutes(config.route);
        this.setIoRoutes(config.route);
    }

    public createExpressApp() {
        this.app = express();
        this.app.use(helmet());
        this.app.use(bodyParser.json());
    }

    public enableAppCors() {
        this.app.use((req: Request, res: Response, next: any) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Auth-Token ");
            next();
        });
    }

    public setPort(port?: number){
        this.app.set("port", process.env.PORT || port || 3000);
    }

    public createServer(app: any) {
        this.server = require('http').createServer(app);
    }

    public createSocket(server: any){
        this.io = require('socket.io')(server);
    }

    public enableIoCors(origin?: string){
        this.io.set('origins', origin || '*:*');
    }

    public setHttpRoutes(routeList: any){
        //loop key values in a foreach to build an api dynamically
        routeList.forEach( (routeInstance: any) => {
            this.app.use('/'+ routeInstance.name, routeInstance.route);
        });
    }

    public setIoRoutes(routeList: any){
        this.io.on("connection", (socket: any) => {
            console.log("a user connected");
            //loop key values in a foreach to build an api dynamically
            routeList.forEach( (routeInstance: any) => {
                socket.on("get" + routeInstance.name, function(data: any) {
                    routeInstance.ioRoute.ioQuery(data)
                });
            });
        });
    }

    public startServer(){
        this.server.listen(this.port, () => console.log('Example app listening on port 3000!'));
    }

    public runTests(logging?: boolean){
        runValidationTests(logging);
    }

}
