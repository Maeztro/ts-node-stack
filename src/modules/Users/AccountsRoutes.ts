import {Request, response, Response} from 'express';
import {newAccountEmailTemplate} from '../../config/config';
import {DTOValidation} from '../DtoValidation/DTOValidation';
import {UserController} from './UserController';

const nodemailer = require ('nodemailer');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const express = require('express');

const transporter = nodemailer.createTransport({
    service: process.env.EMAIL_SERVICE_PROVIDER,
    auth: {
        user: process.env.EMAIL_TRANSPORT,
        pass: process.env.EMAIL_TRANSPORT_PASSWORD
    }
});

// todo : ( modify users ) later when the user has its profile and dashboard implemented , retrieve all the user content -> next
// todo also when email validation is done, add a "forgot password" that reset the password by email -- email is now sent but has no functionalities
// todo maybe also send an email when an account is closed , also at this point might as well move all the user Routes into this one and rename this UserRoutes

export class AccountsRoutes extends express.Router{

    protected userController: UserController;
    protected validation: DTOValidation = new DTOValidation();
    protected userModel: any;

    constructor (config?: any){
        super();

        this.userModel = config.model;
        this.userController = config.itemList;

        this.get(`/getModel`, (req: Request, res: Response) => {
            res.send(`${JSON.stringify(this.userModel)}`);
        });

        this.post('/isAuth' , isAuth , (req: any, res: any) => {
            // console.log("received auth token" , req.token);
            res.json({message:"user is authorized"})

        });

        this.post(`/logout`, (req: Request, res: Response) => {
            console.log("logged out");
            res.send(`logged out`);
        });

        this.post(`/login`, (req: Request, res: Response) => {

            console.log("login request :" ,req.body);

            const credentials = {
                username: req.body.username,
                password: req.body.password,
            };

            this.userController.getByUsername({collection: config.route , username: credentials.username})
                .then(result => {

                    console.log("RESULT!" , result)

                    if ( result.length !== 0) {
                        const retrievedUser = result[0];

                        console.log(retrievedUser.active);

                        if(credentials.username === retrievedUser.username) {
                            bcrypt.compare(credentials.password,retrievedUser.password)
                                .then( result => {
                                    if( result === true ) {

                                        console.log("password match");

                                        jwt.sign( {retrievedUser} , 'secretkey' , {expiresIn: '1h'} ,(err , token) => {

                                            console.log("logged in" , retrievedUser);

                                            const userInfos = {};

                                            for(let [key] of Object.entries(retrievedUser)) {
                                                if(key === "password" || key === "active") {
                                                    //send everything except the password or the active status of the account
                                                } else {
                                                    userInfos[key] = retrievedUser[key];
                                                }
                                            }

                                            res.send({authorization: token , user: userInfos , status: 200 , message: "login successful"});
                                        });

                                    } else {
                                        console.log("wrong password");
                                        res.sendStatus(403);

                                    }
                                })
                        }

                    } else {
                        console.log("USER NOT FOUND");
                        res.send({message:"user not found", status:403});

                    }
                }).catch( error => {
                    console.log("ERROR" , error);
                    res.send({message:"an error occured" , status:500});
                });

        });

        this.post(`/signup`, (req: Request, res: Response) => {

            this.userController.getByUsername({collection: config.route , username: req.body.username})
                .then( result => {
                    if (result.length !== 0){
                        console.log("USER ALREADY EXIST");
                        res.send({message:"user already exist", status:403});
                    } else {
                        this.userController.getByEmail({collection: config.route , username: req.body.email})
                            .then( result => {
                                if ( result !== 0) {
                                    if (this.validation.validateDto( this.userModel,  req.body , true)) {
                                        bcrypt.hash(req.body.password , 12).then( cryptedPassword => {
                                            const newUser = {
                                                username: req.body.username,
                                                email: req.body.email,
                                                password: cryptedPassword,
                                                profile: [
                                                    { name: req.body.username, editable: true, visible:true },
                                                    { userProfileImage: "https://icon-library.com/images/default-profile-icon/default-profile-icon-24.jpg", editable: true, visible:true },
                                                    { description: "", editable: true, visible:true },
                                                    { active: true, editable: true, visible:false },
                                                ],
                                            };

                                            //todo: currently bugged because google

                                            // const mailConfig = {
                                            //     from: process.env.EMAIL_TRANSPORT,
                                            //     // @ts-ignore
                                            //     to: newUser.email,
                                            //     subject: 'Sending Email using Node.js',
                                            //     html: newAccountEmailTemplate
                                            // }

                                            this.userController.create({collection: config.route, value: newUser})
                                                .then(result => {
                                                    // sendEmail(mailConfig);
                                                    res.send({message: "new user created",status: 200});

                                                })
                                                .catch(err => console.log(err));

                                        });
                                    } else {
                                        // console.log("INVALID PAYLOAD" , "MODEL IS: " , this.userModel, "BUT PAYLOAD IS:" , req.body );
                                        res.send({message:"invalid payload" , status:500});
                                    }
                                }

                            });
                    }
                });

        });

        this.put(`/editUser/${config.route}`, isAuth , (req: Request,res: Response) => {

            //todo : eventually validate if the user id match the item id here as well for extra safety

            if(
                this.validation.validateDto( this.itemModel, req.body.oldItem ) &&
                this.validation.validateDto( this.itemModel, req.body.newItem )
            ) {

                const editedItem = {};

                for(let [key] of Object.entries(req.body.newItem)) {
                    if(key === "_id") {
                        //let Mongo deal with the id
                    } else {
                        editedItem[key] = req.body.newItem[key];
                    }
                }

                this.userController.edit({collection: config.route  , oldItem: req.body.oldItem , newItem: editedItem})
                    .then( result => {
                        res.send({status:200});
                    })
                    .catch(err => console.log(err));
            } else {
                res.send({status:500});
            }
        });

        this.post(`/closeAccount` , isAuth , (req: Request,res: Response) => {

            // console.log("CLOSING ACCOUNT", req.body.userId , config.route);
            console.log("CLOSING ACCOUNT", req.body , config.route);

            this.userController.delete({collection: config.route, id: req.body.userId}).then( response => {
                res.send({message: "this account has been closed",status: 200});
            })

        })


    }

}

export const isAuth = (req, res, next) => {
    const bearerheader = req.headers['authorization'];

    if (bearerheader) {
        const bearer = bearerheader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;

        jwt.verify(req.token , process.env.SECRET_KEY ,(err: any, authData: any) => {
            if(err) {
                console.log("not authorized");
                res.sendStatus(403);

            } else {
                console.log("authorized");
                next();

            }
        });

    } else {
        console.log("invalid auth token");
        res.sendStatus(403);

    }
};

export const sendEmail = (mailOptions) => {

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });

}
