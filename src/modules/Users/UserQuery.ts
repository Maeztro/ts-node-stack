export class UserQuery {
    collection:string;
    id?:string;
    username?:string;
    email?:string;
}
