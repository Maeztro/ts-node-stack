import {ObjectId} from 'mongodb';
import {ItemQuery} from '../Items/ItemQuery';
import {ItemController} from '../Items/ItemController';
import {UserQuery} from './UserQuery';

export class UserController extends ItemController {
    constructor(database){
        super(database);
        this.database = database;
    }

    public getByUsername(query: UserQuery):any {
        console.log("GET ITEM RECIEVED",query);
        return this.database.collection(query.collection).find({username:query.username}).toArray();
    }

    public getByEmail(query: UserQuery):any {
        // console.log("GET ITEM RECIEVED",query);
        return this.database.collection(query.collection).find({email:query.email}).toArray();
    }

    public edit(query: any):any {
        // console.log("EDIT RECIEVED",query.oldItem , query.newItem);
        console.log("OLD ITEM ID: " , query.oldItem._id);
        return this.database.collection(query.collection).updateOne({_id:new ObjectId(query.oldItem._id)}, {$set: query.newItem});
    }

    public delete(query: UserQuery):any {
        // console.log("DELETE RECIEVED", query.id , query.collection);
        return this.database.collection(query.collection).deleteOne({_id: new ObjectId(query.id)});
    }

}
