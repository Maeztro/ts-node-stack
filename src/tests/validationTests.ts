import {accountModel, itemModel, nestedModel, stressModel, userProfileModel} from '../config/config';
import {DTOValidation} from '../modules/DtoValidation/DTOValidation';

/**
 *  TESTS!
 *  this is a simple implementation that does not use any test suites
 *  maybe later one should be used but for now this is fine and dandy
 * */

const testAccountModel= accountModel;

const testAccount = {
    _id: "1",
    username: "bob",
    email: "email@email.com",
    password: "1234",
    profile: [
        { name: "bob", editable: true, visible:true },
        { userProfileImage: "cat.jpg", editable: true, visible:true },
        { description: "yes he can", editable: true, visible:true },
        { active: true, editable: true, visible:false },
    ]
};

const testItemModel = itemModel;

const testItem = {
        _id: "478943567834965437896543789",
        username: "bob",
        userId: "3t46723567325627857",
        userProfileImage: "lol",
        upVotes: 1,
        downVotes: 9999,
        editMode: true,
        content: {
            title: "bob",
            subtitle: "lol",
            url: "somewebsite.com",
            link: "someotherwebsite.com",
            description: "a thing",
            category: "things",
        }
    }

const stressItemModel = stressModel;

const stressItem = {
    _id: "1",
    username: "bob",
    email: "email@email.com",
    password: "1234",
    content : testItem.content,
    insanity: [
        testItem.content
    ]
};

const nestedItemModel = nestedModel;

export const nestedItem = {
    singleField: "bob",
    arrayField:[
        {singleField: "bob"}
    ],
    objectField:{
        singleField:"bob"
    },
    arrayFieldWithNestedArray:[
        [
            {singleField: "bob"}
        ]
    ],
    objectFieldWithNestedArray:{
        singleField:[
            {singleField: "bob"}
        ]
    },
    objectFieldWithNestedObjectField:{
        singleField: {singleField: "bob"}
    },
}


const testValidation: DTOValidation = new DTOValidation();

export function runValidationTests(logging?: boolean) {

    // /**
    //  *  TEST ITEM VALIDATION
    //  * */
    //
    // if(testValidation.validateDto(testItemModel , testItem, logging)){
    //     console.log("VALIDATION PASSED");
    // } else {
    //     console.log("VALIDATION FAILED");
    // }
    //

    // /**
    //  *  TEST USER VALIDATION
    //  * */
    //
    // if(testValidation.validateDto(testAccountModel , testAccount, logging)){
    //     console.log("VALIDATION PASSED");
    // } else {
    //     console.log("VALIDATION FAILED");
    // }

    // /**
    //  *  STRESS TEST!
    //  * */
    //
    // if(testValidation.validateDto(stressItemModel , stressItem, logging)){
    //     console.log("VALIDATION PASSED");
    // } else {
    //     console.log("VALIDATION FAILED");
    // }

    /**
     *  NESTED TEST!
     * */

    if(testValidation.validateDto(nestedItemModel , nestedItem, true)){
        console.log("VALIDATION PASSED");
    } else {
        console.log("VALIDATION FAILED");
    }

}