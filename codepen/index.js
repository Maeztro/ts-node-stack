const socket = io('http://localhost:3000');

const buttonDiceWebsocket = document.querySelector("#getDiceWebsocket");
const buttonItemWebsocket = document.querySelector("#getItemWebsocket");
const buttonItemHttp = document.querySelector("#getItemHttp");
const buttonDiceHttp = document.querySelector("#getDiceHttp");
const buttonPostItemHttp = document.querySelector("#postItemHttp");
const diceRollInput = document.querySelector("#diceRollInput");
const diceSizeInput = document.querySelector("#diceSizeInput");
const itemIdInput = document.querySelector("#itemIdInput");
const queryItemInput = document.querySelector("#queryItemInput");
const queryDiceRollInput = document.querySelector("#queryDiceRollInput");
const queryDiceSizeInput = document.querySelector("#queryDiceRollInput");

buttonItemHttp.addEventListener("click" , () => {
	let itemIdQuery = queryItemInput.value
	fetch(`http://localhost:3000/items/item${itemIdQuery}`)
	.then( res => res.json())
	.then( res => console.log("RES:" , res));
});

buttonDiceHttp.addEventListener("click" , () => {
	let diceRollQuery = queryDiceRollInput.value
	let diceSizeQuery = queryDiceSizeInput.value
	fetch(`http://localhost:3000/dices/roll${diceRollQuery}/d${diceSizeQuery}`)
	.then( res => res.json())
	.then( res => console.log("RES:" , res));
});

buttonDiceWebsocket.addEventListener("click" , () => {
	let diceRollInputQuery = diceRollInput.value;
	let diceSizeInputQuery = diceSizeInput.value;
	socket.emit('getdices', {roll: diceRollInputQuery, size: diceSizeInputQuery})
	console.log("SENDING MESSAGE" , {roll: diceRollInputQuery, size: diceSizeInputQuery})
});

buttonItemWebsocket.addEventListener("click" , () => {
	let itemIdInputQuery = itemIdInput.value;
	socket.emit('getitems', {id:itemIdInputQuery})
	console.log("SENDING MESSAGE" , {id:itemIdInputQuery})
});

buttonPostItemHttp.addEventListener("click" , () => {
	const newItem = {name: "item6", value: "a box full of kittens"}

	fetch(`http://localhost:3000/items/addItem`,
		{
			method: 'POST',
			body: JSON.stringify(newItem),
			headers: {
				'Content-Type': 'application/json'
			}
		})
	.then( res => res.json())
	.then( res => console.log("RES:" , res));
});

//https://jsfiddle.net/s8xrcj3o/9/
